#
# AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2015-09-09 18:47+0200\n"
"PO-Revision-Date: 2017-09-06 17:18+0000\n"
"Last-Translator: Andika Triwidada <andika@gmail.com>\n"
"Language-Team: Indonesian <https://hosted.weblate.org/projects/debian-handbook/revision_history/id/>\n"
"Language: id-ID\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.17-dev\n"

msgid "Revision History"
msgstr "Riwayat Revisi"

msgid "Raphaël"
msgstr "Raphaël"

msgid "Hertzog"
msgstr "Hertzog"
