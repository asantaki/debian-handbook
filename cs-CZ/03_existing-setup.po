msgid ""
msgstr "Project-Id-Version: 0\nPOT-Creation-Date: 2022-07-30 18:23+0200\nPO-Revision-Date: 2022-09-16 21:20+0000\nLast-Translator: Allan Nordhøy <epost@anotheragency.no>\nLanguage-Team: Czech <https://hosted.weblate.org/projects/debian-handbook/03_existing-setup/cs/>\nLanguage: cs-CZ\nMIME-Version: 1.0\nContent-Type: application/x-publican; charset=UTF-8\nContent-Transfer-Encoding: 8bit\nPlural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\nX-Generator: Weblate 4.14.1-dev\n"

msgid "Existing Setup"
msgstr "Současné systémové nastavení"

msgid "Reuse"
msgstr "Znovupoužití"

msgid "Migration"
msgstr "Přechod"

msgid "Analyzing the Existing Setup and Migrating"
msgstr "Analýza současného systémového nastavení a přechodu"

msgid "Any computer system overhaul should take the existing system into account. This allows reuse of available resources as much as possible and guarantees interoperability of the various elements comprising the system. This study will introduce a generic framework to follow in any migration of a computing infrastructure to Linux."
msgstr "Každá počítačová generálka by měla zohledňovat současný systém. To umožní znovupoužití dostupných zdrojů jak je to jen možné a zabezpečí vzájemnou provozuschopnost různých prvků obsahujících systém. Tato studie představí obecný rámec, který by se měl dodržet při jakémkoliv přechodu počítačové infrastruktury na Linux."

msgid "Coexistence in Heterogeneous Environments"
msgstr "Spoluexistence v různorodém prostředí"

msgid "<primary>environment</primary><secondary>heterogeneous environment</secondary>"
msgstr "<primary>prostředí</primary><secondary>různorodé prostředí</secondary>"

msgid "Debian integrates very well in all types of existing environments and plays well with any other operating system. This near-perfect harmony comes from market pressure which demands that software publishers develop programs that follow standards. Compliance with standards allows administrators to switch out programs: clients or servers, whether free or not."
msgstr "Debian se začleňuje velmi dobře do všech typů současných prostředí a rozumí si dobře s jakýmkoliv jiným operačním systémem. Tento téměř dokonalý soulad si svým tlakem vyžádal trh, který požaduje, aby vydavatelé softwaru vyvíjeli programy, které vyhovují standardům. Soulad se standardy dovoluje správcům přepínat programy: klienty nebo servry, ať už volné, či ne."

msgid "Integration with Windows Machines"
msgstr "Začlenění s Windows zařízeními"

#, fuzzy
#| msgid "Samba's SMB/CIFS support ensures excellent communication within a Windows context. It shares files and print queues to Windows clients and includes software that allow a Linux machine to use resources available on Windows servers."
msgid "Samba's SMB/CIFS support ensures excellent communication within a Windows context. It shares files and print queues to Windows clients and includes software that allows a Linux machine to use resources available on Windows servers."
msgstr "Podpora Samby SMB/CIFS zajišťuje vynikající komunikaci v rámci souvislostí s Windows. Sdílí soubory a tiskové fronty klientům Windows a zahrnuje software, který umožňuje linuxovským zařízením používat zdroje dostupné na serverech Windows."

msgid "<emphasis>TOOL</emphasis> Samba"
msgstr "<emphasis>NÁSTROJ</emphasis> Samba"

msgid "<primary>Samba</primary>"
msgstr "<primary>Samba</primary>"

msgid "The latest version of Samba can replace most of the Windows features: from those of a simple Windows NT server (authentication, files, print queues, downloading printer drivers, DFS, etc.) to the most advanced one (a domain controller compatible with Active Directory)."
msgstr "Poslendí verze Samby může nahradit většinu prvků Windows: od těch z jednoduchého serveru Windows NT (autentizace, soubory, tiskové fronty, stahování ovladačů tiskárny, DFS, apod.) až k těm nejpokročilejším (doménový ovladač kompatabilní s aktivním adresářem)."

#, fuzzy
#| msgid "Integration with OS X machines"
msgid "Integration with macOS machines"
msgstr "Začlenění se zařízeními OS X"

msgid "<primary>Zeroconf</primary>"
msgstr "<primary>Zeroconf</primary>"

msgid "<primary>Bonjour</primary>"
msgstr "<primary>Bonjour</primary>"

msgid "<primary>Avahi</primary>"
msgstr "<primary>Avahi</primary>"

#, fuzzy
#| msgid "OS X machines provide, and are able to use, network services such as file servers and printer sharing. These services are published on the local network, which allows other machines to discover them and make use of them without any manual configuration, using the Bonjour implementation of the Zeroconf protocol suite. Debian includes another implementation, called Avahi, which provides the same functionality."
msgid "macOS machines provide, and are able to use, network services such as file servers and printer sharing. These services are published on the local network, which allows other machines to discover them and make use of them without any manual configuration, using the Bonjour implementation of the Zeroconf protocol suite. Debian includes another implementation, called Avahi, which provides the same functionality."
msgstr "Zařízení OS X poskytují, a mohou používat, síťové služby jako jsou souborové servery a sdílení tiskáren. Tyto služby jsou zveřejněny na lokální síti, což umožňuje ostatním zařízením je prozkoumat a mít prospěch z jejich používání bez nějakého manuálního nastavování, za použití zavedení Bonjour z protokolové sady Zeroconf. Debian zahrnje jiné zavedení, které se nazývá Avahi a poskytuje ten samý rozsah funkcí."

msgid "<primary>AFP</primary><seealso>Apple Filing Protocol</seealso>"
msgstr ""

#, fuzzy
#| msgid "<primary>AppleTalk</primary>"
msgid "<primary>Apple Filing Protocol</primary>"
msgstr "<primary>AppleTalk</primary>"

msgid "<primary>AppleShare</primary>"
msgstr "<primary>AppleShare</primary>"

#, fuzzy
#| msgid "In the other direction, the Netatalk daemon can be used to provide file servers to OS X machines on the network. It implements the AFP (AppleShare) protocol as well as the required notifications so that the servers can be autodiscovered by the OS X clients."
msgid "In the other direction, the Netatalk daemon can be used to provide file servers to macOS machines on the network. It implements the AFP protocol (Apple Filing Protocol, now AppleShare) as well as the required notifications so that the servers can be automatically discovered by the macOS clients."
msgstr "Opačným směrem, Netatalk démon může být použit k poskytování souborových serverů vůči zařízením OS X v síti. Zavádí AFP (AppleShere) protokol stejně jako požadovaná oznámení, takže servery mohou být automaticky rozpoznány klienty OS X."

msgid "<primary>AppleTalk</primary>"
msgstr "<primary>AppleTalk</primary>"

msgid "Older Mac OS networks (before OS X) used a different protocol called AppleTalk. For environments involving machines using this protocol, Netatalk also provides the AppleTalk protocol (in fact, it started as a reimplementation of that protocol). It ensures the operation of the file server and print queues, as well as time server (clock synchronization). Its router function allows interconnection with AppleTalk networks."
msgstr "Starší sítě Mac OS (před OS X) používaly odlišné protokoly nazývané AppleTalk. Pro prostředí zahrnující zařízení používající tento protokol, Netatalk také poskytuje protokol AppleTalk (v podstatě, začal jako znovuzavedení tohoto protokolu). To zabezpečuje funkčnost souborového serveru a tiskových front, stejně jako časového serveru (časové sladění hodin). Jeho směrovací funkce umožňuje vzájemné spojení se sítěmi AppleTalk."

msgid "Integration with Other Linux/Unix Machines"
msgstr "Začlenění s ostatními zařízeními Linux/Unix"

msgid "<primary>NFS</primary><seealso>Network File System</seealso>"
msgstr ""

#, fuzzy
#| msgid "<primary>architecture</primary>"
msgid "<primary>Network File System</primary>"
msgstr "<primary>architektura</primary>"

msgid "<primary>NIS</primary><seealso>Network Information Service</seealso>"
msgstr ""

#, fuzzy
#| msgid "<primary>taking over a Debian server</primary>"
msgid "<primary>Network Information Service</primary>"
msgstr "<primary>převzetí Debianového serveru</primary>"

#, fuzzy
#| msgid "Finally, NFS and NIS, both included, guarantee interaction with Unix systems. NFS ensures file server functionality, while NIS creates user directories. The BSD printing layer, used by most Unix systems, also allows sharing of print queues."
msgid "Finally, NFS (Network File System) and NIS (Network Information Service), both included, guarantee interaction with Unix systems. NFS ensures file server functionality, while NIS creates user directories. The BSD printing layer, used by most Unix systems, also allows sharing of print queues."
msgstr "A nakonec, NFS a NIS, obě zahrnuté, zaručují spolupráci s Unixovými systémy. NFS zabezpečuje funkčnost souborového serveru, zatímco NIS vytváří uživatelské adresáře. Tisková vrstva BSD, používaná většinou Unixových systémů, také umožňuje sdílení tiskových front."

msgid "Coexistence of Debian with OS X, Windows and Unix systems"
msgstr "Soužití Debianu s OS X, Windows a systémy Unix"

msgid "How To Migrate"
msgstr "Jak přejít"

msgid "<primary>migration</primary>"
msgstr "<primary>přechod</primary>"

#, fuzzy
#| msgid "In order to guarantee continuity of the services, each computer migration must be planned and executed according to the plan. This principle applies whatever the operating system used."
msgid "In order to guarantee continuity of the services, each computer migration must be planned and executed according to the plan. This principle applies regardless of which operating system is used."
msgstr "Za účelem pokračování služeb bez přerušení, musí být každý počítačový přechod naplánován a proveden podle plánu. Tato zásada platí pro použití jakéhokoliv operačního systému."

msgid "Survey and Identify Services"
msgstr "Průzkum a identifikace služeb"

msgid "As simple as it seems, this step is essential. A serious administrator truly knows the principal roles of each server, but such roles can change, and sometimes experienced users may have installed “wild” services. Knowing that they exist will at least allow you to decide what to do with them, rather than delete them haphazardly."
msgstr "Jakkoliv se tento krok zdá jednoduchý, je zásadní. Správný správce skutečně zná základní funkce každého serveru, ale takové funkce se mohou změnit a někdy i zkušení uživatelé mohli nainstalovat “divoké” služby. Vědomí, že existují, vám může minimálně umožnit se rozhodnout jak s nimi naložit, spíše, než je nahodile vymazat."

msgid "For this purpose, it is wise to inform your users of the project before migrating the server. To involve them in the project, it may be useful to install the most common free software programs on their desktops prior to migration, which they will come across again after the migration to Debian; LibreOffice and the Mozilla suite are the best examples here."
msgstr "Za tímto účelem je moudré před přechodem serveru informovat uživatele o projektu. K jejich zapojení do projektu může být užitečné nainstalovat nejběžnější programy volného softwaru na jejich počítače ještě před přechodem, programy, se kterým přijdou do styku znovu po přechodu na Debian; LibreOffice a sada Mozilla jsou zde nejlepšími příklady"

msgid "Network and Processes"
msgstr "Síť a procesy"

msgid "<primary><command>nmap</command></primary>"
msgstr "<primary><command>nmap</command></primary>"

#, fuzzy
#| msgid "<primary>environment</primary><secondary>heterogeneous environment</secondary>"
msgid "<primary>network</primary><secondary><command>nmap</command></secondary>"
msgstr "<primary>prostředí</primary><secondary>různorodé prostředí</secondary>"

msgid "The <command>nmap</command> tool (in the package with the same name) will quickly identify Internet services hosted by a network connected machine without even requiring to log in to it. Simply call the following command on another machine connected to the same network:"
msgstr "Nástroj <command>nmap</command> (v balíčku pod stejným jménem) rychle rozpozná internetové služby nacházející se na zařízení připojeného k síti bez toho, aniž by se k němu přihlásil. Jednoduše spusťte následující příkaz na jiném zařízení připojeného ke stejné síti:"

#, fuzzy
#| msgid ""
#| "\n"
#| "<computeroutput>$ </computeroutput><userinput>nmap mirwiz</userinput>\n"
#| "<computeroutput>Starting Nmap 6.47 ( http://nmap.org ) at 2015-03-24 11:34 CET\n"
#| "Nmap scan report for mirwiz (192.168.1.104)\n"
#| "Host is up (0.0037s latency).\n"
#| "Not shown: 999 closed ports\n"
#| "PORT   STATE SERVICE\n"
#| "22/tcp open  ssh\n"
#| "\n"
#| "Nmap done: 1 IP address (1 host up) scanned in 0.13 seconds</computeroutput>"
msgid ""
"\n"
"<computeroutput>$ </computeroutput><userinput>nmap mirwiz</userinput>\n"
"<computeroutput>Starting Nmap 7.80 ( https://nmap.org ) at 2021-04-29 14:41 CEST\n"
"Nmap scan report for mirwiz (192.168.1.104)\n"
"Host is up (0.00062s latency).\n"
"Not shown: 992 closed ports\n"
"PORT     STATE SERVICE\n"
"22/tcp   open  ssh\n"
"25/tcp   open  smtp\n"
"80/tcp   open  http\n"
"111/tcp  open  rpcbind\n"
"139/tcp  open  netbios-ssn\n"
"445/tcp  open  microsoft-ds\n"
"5666/tcp open  nrpe\n"
"9999/tcp open  abyss\n"
"\n"
"Nmap done: 1 IP address (1 host up) scanned in 0.06 seconds</computeroutput>"
msgstr ""
"\n"
"<computeroutput>$ </computeroutput><userinput>nmap mirwiz</userinput>\n"
"<computeroutput>Starting Nmap 6.47 ( http://nmap.org ) at 2015-03-24 11:34 CET\n"
"Nmap scan report for mirwiz (192.168.1.104)\n"
"Host is up (0.0037s latency).\n"
"Not shown: 999 closed ports\n"
"PORT   STATE SERVICE\n"
"22/tcp open  ssh\n"
"\n"
"Nmap done: 1 IP address (1 host up) scanned in 0.13 seconds</computeroutput>"

#, fuzzy
#| msgid "<emphasis>ALTERNATIVE</emphasis> Use <command>netstat</command> to find the list of available services"
msgid "<emphasis>ALTERNATIVE</emphasis> Use <command>ss</command> to find the list of available services"
msgstr "<emphasis>ALTERNATIVA</emphasis> Použití <command>netstatu</command> k nalezení seznamu dostupných služeb"

#, fuzzy
#| msgid "<primary>environment</primary><secondary>heterogeneous environment</secondary>"
msgid "<primary>network</primary><secondary><command>ss</command></secondary>"
msgstr "<primary>prostředí</primary><secondary>různorodé prostředí</secondary>"

#, fuzzy
#| msgid "On a Linux machine, the <command>netstat -tupan</command> command will show the list of active or pending TCP sessions, as well UDP ports on which running programs are listening. This facilitates identification of services offered on the network."
msgid "On a Linux machine, the <command>ss -anptu</command> command will show the list of active or pending TCP sessions, as well as UDP ports on which running programs are listening. This facilitates identification of services offered on the network."
msgstr "Na Linuxových zařízeních, příkaz <command>netstat -tupan</command> ukáže seznam aktivních nebo dosud nevyřízených TCP akcí, stejně jako UDP rozhraní (portů), na kterých běžící programy čekají na odezvu. To usnadňuje identifikaci služeb nabízených sítí."

msgid "<emphasis>GOING FURTHER</emphasis> IPv6"
msgstr "<emphasis>JDEME DÁLE</emphasis> IPv6"

#, fuzzy
#| msgid "<primary>environment</primary><secondary>heterogeneous environment</secondary>"
msgid "<primary>network</primary><secondary>IPv6</secondary>"
msgstr "<primary>prostředí</primary><secondary>různorodé prostředí</secondary>"

#, fuzzy
#| msgid "Some network commands may work either with IPv4 (the default usually) or with IPv6. These include the <command>nmap</command> and <command>netstat</command> commands, but also others, such as <command>route</command> or <command>ip</command>. The convention is that this behavior is enabled by the <parameter>-6</parameter> command-line option."
msgid "Some network commands may work either with IPv4 (the default usually) or with IPv6. These include the <command>nmap</command> and <command>ss</command> commands, but also others, such as <command>route</command> or <command>ip</command>. The convention is that this behavior is enabled by the <parameter>-6</parameter> command-line option."
msgstr "Některé síťové příkazy mohou fungovat buď s IPv4 (obyčejně výchozí) nebo s IPv6. Mezi ně patří příkazy <command>nmap</command> a <command>netstat</command>, ale také další, jako <command>route</command> nebo <command>ip</command>. Je zvykem, že toto chování je umožněno <parameter>-6</parameter> volbou na příkazovém řádku."

#, fuzzy
#| msgid "<primary><command>nmap</command></primary>"
msgid "<primary><command>ps</command></primary>"
msgstr "<primary><command>nmap</command></primary>"

#, fuzzy
#| msgid "<primary><command>nmap</command></primary>"
msgid "<primary><command>who</command></primary>"
msgstr "<primary><command>nmap</command></primary>"

#, fuzzy
#| msgid "<primary>environment</primary><secondary>heterogeneous environment</secondary>"
msgid "<primary>system</primary><secondary><command>ps</command></secondary>"
msgstr "<primary>prostředí</primary><secondary>různorodé prostředí</secondary>"

#, fuzzy
#| msgid "<primary>environment</primary><secondary>heterogeneous environment</secondary>"
msgid "<primary>system</primary><secondary><command>who</command></secondary>"
msgstr "<primary>prostředí</primary><secondary>různorodé prostředí</secondary>"

#, fuzzy
#| msgid "If the server is a Unix machine offering shell accounts to users, it is interesting to determine if processes are executed in the background in the absence of their owner. The command <command>ps auxw</command> displays a list of all processes with their user identity. By checking this information against the output of the <command>who</command> command, which gives a list of logged in users, it is possible to identify rogue or undeclared servers or programs running in the background. Looking at <filename>crontabs</filename> (tables listing automatic actions scheduled by users) will often provide interesting information on functions fulfilled by the server (a complete explanation of <command>cron</command> is available in <xref linkend=\"sect.task-scheduling-cron-atd\" />)."
msgid "If the server is a Unix machine offering shell accounts to users, it is interesting to determine if processes are executed in the background in the absence of their owner. The command <command>ps auxw</command> displays a list of all processes with their user identity. By checking this information against the output of the <command>who</command> or <command>w</command> commands, which give a list of logged in users, it is possible to identify rogue or undeclared servers or programs running in the background. Looking at <filename>crontabs</filename> (tables listing automatic actions scheduled by users) will often provide interesting information on functions fulfilled by the server (a complete explanation of <command>cron</command> is available in <xref linkend=\"sect.task-scheduling-cron-atd\" />)."
msgstr "Pokud je serverem Unixové zařízení nabízející uživatelům shellové účty, je zajímavé určit, zda jsou procesy prováděny na pozadí za nepřítomnosti svých majitelů. Příkaz <command>ps auxw</command> zobrazuje seznam všech procesů s jejich uživatelskou totožností. Kontrolou těchto informací s výstupem příkazu <command>who</command>, který poskytuje seznam přihlášených uživatelů, je možné identifikovat škodlivé nebo nedeklarované servery nebo programy běžící na pozadí. Pohledem na <filename>crontabs</filename> (tabulka vypisující automatické akce naplánované uživateli) často poskytuje zajímavé informace o funkcích naplňovaných serverem (kompletní vysvětlení <command>cron</command> je k dispozici na <xref linkend=\"sect.task-scheduling-cron-atd\" />)."

msgid "In any case, it is essential to backup your servers: this allows recovery of information after the fact, when users will report specific problems due to the migration."
msgstr "V každém případě ja důležité zálohovat vaše servery: to umožní navrácení informací poté, co budou uživatelé hlásit určité problémy po přechodu."

msgid "Backing up the Configuration"
msgstr "Zálohování nastavení"

msgid "It is wise to retain the configuration of every identified service in order to be able to install the equivalent on the updated server. The bare minimum is to make a backup copy of the configuration files."
msgstr "Je moudré ponechat si nastavení každé rozpoznané služby kvůli tomu, abychom byli schopni nainstalovat obdobu na aktualizovaný server. Nutné minimum je si udělat zálohovou kopii souborů nastavení."

#, fuzzy
#| msgid "<primary>migration</primary>"
msgid "<primary><filename>/etc</filename></primary>"
msgstr "<primary>přechod</primary>"

#, fuzzy
#| msgid "<primary>environment</primary><secondary>heterogeneous environment</secondary>"
msgid "<primary><filename>/usr</filename></primary><secondary><filename>/usr/local/</filename></secondary>"
msgstr "<primary>prostředí</primary><secondary>různorodé prostředí</secondary>"

#, fuzzy
#| msgid "<primary>migration</primary>"
msgid "<primary><filename>/opt</filename></primary>"
msgstr "<primary>přechod</primary>"

msgid "For Unix machines, the configuration files are usually found in <filename>/etc/</filename>, but they may be located in a sub-directory of <filename>/usr/local/</filename>. This is the case if a program has been installed from sources, rather than with a package. In some cases, one may also find them under <filename>/opt/</filename>."
msgstr "Pro Unixová zařízení jsou soubory nastavení obyčejně nalezeny v <filename>/etc/</filename>, ale mohou být umístěny i v podadresáři <filename>/usr/local/</filename>. Toto je případ, pokud byl program nainstalován ze zdrojových souborů a ne za pomoci balíčku. V některých případech se mohou nalézat také pod <filename>/opt/</filename>."

msgid "For data managing services (such as databases), it is strongly recommended to export the data to a standard format that will be easily imported by the new software. Such a format is usually in text mode and documented; it may be, for example, an SQL dump for a database, or an LDIF file for an LDAP server."
msgstr "Pro služby obhospodařující data (jako jsou databáze) se důrazně doporučuje vyexportovat data do standardního formátu, který se snadno vimportuje novým softwarem. Takový formát je obvykle v textovém režimu a je zdokumentován; může to být například výpis SQL pro databáze nebo soubor LDIF pro servery LDAP."

msgid "Database backups"
msgstr "Zálohy databáze"

msgid "Each server software is different, and it is impossible to describe all existing cases in detail. Compare the documentation for the existing and the new software to identify the exportable (thus, re-importable) portions and those which will require manual handling. Reading this book will clarify the configuration of the main Linux server programs."
msgstr "Každý serverový sofrware je jiný a je nemožné popsat všechny existující případy do detailu. Srovnejte dokumentaci současného a nového sofrwaru a identifikujte části, které je možno exportovat (tedy re-importovat) a části, které budou vyžadovat manuální činnost. Čtení této knihy objasní nastavení hlavních programů Linuxového serveru."

msgid "Taking Over an Existing Debian Server"
msgstr "Převzetí současného Debianového serveru"

msgid "<primary>recovering a Debian machine</primary>"
msgstr "<primary>obnovení zařízení Debianového zařízení</primary>"

msgid "<primary>exploring a Debian machine</primary>"
msgstr "<primary>prozkoumání Debianového zařízení</primary>"

msgid "<primary>taking over a Debian server</primary>"
msgstr "<primary>převzetí Debianového serveru</primary>"

msgid "To effectively take over its maintenance, one may analyze a machine already running with Debian."
msgstr "K efektivnímu převzetí údržby se dá zanalyzovat zařízení, na kterém už Debian běží."

#, fuzzy
#| msgid "<primary>environment</primary><secondary>heterogeneous environment</secondary>"
msgid "<primary><filename>/etc</filename></primary><secondary><filename>/etc/debian_version</filename></secondary>"
msgstr "<primary>prostředí</primary><secondary>různorodé prostředí</secondary>"

#, fuzzy
#| msgid "<primary>migration</primary>"
msgid "<primary><filename>debian_version</filename></primary>"
msgstr "<primary>přechod</primary>"

msgid "The first file to check is <filename>/etc/debian_version</filename>, which usually contains the version number for the installed Debian system (it is part of the <emphasis>base-files</emphasis> package). If it indicates <literal><replaceable>codename</replaceable>/sid</literal>, it means that the system was updated with packages coming from one of the development distributions (either testing or unstable)."
msgstr "Prvním souborem ke kontrole by měl být <filename>/etc/debian_version</filename>, který obvykle obsahuje číslo verze nainstalovaného systému Debianu (je součástí balíčku <emphasis>base-files</emphasis>). Pokud ukazuje <literal><replaceable>kódové jméno</replaceable>/sid</literal>, znamená to, že systém byl aktualizovaný s balíčky pocházejících z jedné z vývojových distribucí (buď testovací nebo nestabilní)."

msgid "The <command>apt-show-versions</command> program (from the Debian package of the same name) checks the list of installed packages and identifies the available versions. <command>aptitude</command> can also be used for these tasks, albeit in a less systematic manner."
msgstr "Program <command>apt-show-versions</command> (z balíčku Debianu toho samého jména) kontroluje seznam nainstalovaných balíčků a rozpoznává dostupné verze. <command>aptitude</command> může také zastat takové úkoly, třebaže méně systematickým způsobem."

#, fuzzy
#| msgid "<primary>architecture</primary>"
msgid "<primary><filename>sources.list</filename></primary>"
msgstr "<primary>architektura</primary>"

msgid "A glance at the <filename>/etc/apt/sources.list</filename> file (and <filename>/etc/apt/sources.list.d/</filename> directory) will show where the installed Debian packages likely came from. If many unknown sources appear, the administrator may choose to completely reinstall the computer's system to ensure optimal compatibility with the software provided by Debian."
msgstr "Nahlédnutí do souboru <filename>/etc/apt/sources.list</filename> (a adresáře <filename>/etc/apt/sources.list.d/</filename>) umožní zjistit, odkud nainstalované balíčky Debianu pravděpodobně pocházejí. Pokud se zde objeví hodně neznámých zdrojů, může správce zvolit kompletní reinstalaci počítačového systmému za účelem zajištění optimální kompatability se softwarem poskytovaným Debianem."

#, fuzzy
#| msgid "The <filename>sources.list</filename> file is often a good indicator: the majority of administrators keep, at least in comments, the list of APT sources that were previously used. But you should not forget that sources used in the past might have been deleted, and that some random packages grabbed on the Internet might have been manually installed (with the <command>dpkg</command> command). In this case, the machine is misleading in its appearance of “standard” Debian. This is why you should pay attention to any indication that will give away the presence of external packages (appearance of <filename>deb</filename> files in unusual directories, package version numbers with a special suffix indicating that it originated from outside the Debian project, such as <literal>ubuntu</literal> or <literal>lmde</literal>, etc.)"
msgid "The <filename>sources.list</filename> file is often a good indicator: the majority of administrators keep, at least in comments, the list of APT sources that were previously used. But you should not forget that sources used in the past might have been deleted, and that some random packages grabbed on the Internet might have been manually installed (with the help of the <command>dpkg</command> command). In this case, the machine is misleading in its appearance of being a “standard” Debian system. This is why you should pay attention to any indication that will give away the presence of external packages (appearance of <filename>deb</filename> files in unusual directories, package version numbers with a special suffix indicating that it originated from outside the Debian project, such as <literal>ubuntu</literal> or <literal>lmde</literal>, etc.)."
msgstr "Soubor <filename>sources.list</filename> je často dobrým ukazatelem: většina správců si uchovává, minimálně formou komentářů, seznam zdrojů APT, které byly dříve použity. Neměli by jste ovšem zapomínat, že zdroje použité v minulosti mohly být vymazány a že některé náhodné balíčky stáhnuté přes internet mohly být manuálně nainstalované (pomocí příkazu <command>dpkg</command>). V tomto případě zařízení ve svém vzezření “standardního” Debian klame. To je důvod, proč by jste si měli dávat pozor na jakékoliv náznaky, které prozradí přítomnost externích balíčků (výskyt <filename>deb</filename> souborů v neobvyklých adresářích, čísla verzí balíčků se speciální příponou, která naznačuje, že balíček pochází z postředí mimo projekt Debian, jako je <literal>ubuntu</literal> nebo <literal>lmde</literal>, apod.)"

msgid "Likewise, it is interesting to analyze the contents of the <filename>/usr/local/</filename> directory, whose purpose is to contain programs compiled and installed manually. Listing software installed in this manner is instructive, since this raises questions on the reasons for not using the corresponding Debian package, if such a package exists."
msgstr "Podobně je zajímavé analyzovat obsah adresáře <filename>/usr/local/</filename>, jehož úkolem je shraňovat programy ručně přeložené a nainstalované. Vypsání seznamu softwaru nainstalovaného tímto způsobem je poučné, protože to podněcuje otázky na důvody nepoužití odpovídajícího balíčků Deabianu, podud takový existuje."

msgid "<emphasis>QUICK LOOK</emphasis> <emphasis role=\"pkg\">cruft</emphasis>/<emphasis role=\"pkg\">cruft-ng</emphasis>, <emphasis role=\"pkg\">debsums</emphasis>, and <emphasis role=\"pkg\">apt-show-versions</emphasis>"
msgstr ""

#, fuzzy
#| msgid "<emphasis>QUICK LOOK</emphasis> <emphasis role=\"pkg\">cruft</emphasis>"
msgid "<primary><emphasis role=\"pkg\">cruft</emphasis></primary>"
msgstr "<emphasis>RYCHLÝ NÁHLED</emphasis> <emphasis role=\"pkg\">cruft</emphasis>"

#, fuzzy
#| msgid "<emphasis>QUICK LOOK</emphasis> <emphasis role=\"pkg\">cruft</emphasis>"
msgid "<primary><emphasis role=\"pkg\">cruft-ng</emphasis></primary>"
msgstr "<emphasis>RYCHLÝ NÁHLED</emphasis> <emphasis role=\"pkg\">cruft</emphasis>"

#, fuzzy
#| msgid "<emphasis>QUICK LOOK</emphasis> <emphasis role=\"pkg\">cruft</emphasis>"
msgid "<primary><emphasis role=\"pkg\">debsums</emphasis></primary>"
msgstr "<emphasis>RYCHLÝ NÁHLED</emphasis> <emphasis role=\"pkg\">cruft</emphasis>"

msgid "<primary><emphasis role=\"pkg\">apt-show-versions</emphasis></primary>"
msgstr ""

#, fuzzy
#| msgid "The <emphasis role=\"pkg\">cruft</emphasis> package proposes to list the available files that are not owned by any package. It has some filters (more or less effective, and more or less up to date) to avoid reporting some legitimate files (files generated by Debian packages, or generated configuration files not managed by <command>dpkg</command>, etc.)."
msgid "The <emphasis role=\"pkg\">cruft</emphasis> and <emphasis role=\"pkg\">cruft-ng</emphasis> packages propose to list the available files that are not owned by any package. They have some filters (more or less effective, and more or less up to date) to avoid reporting some legitimate files (files generated by Debian packages, or generated configuration files not managed by <command>dpkg</command>, etc.)."
msgstr "Balíček <emphasis role=\"pkg\">cruft</emphasis> přichází s vypsáním dostupných souborů, které nepatří žádnému balíčku. Má filtry (více či méně účinné a více či méně aktuální) k tomu, aby předešel hlášení legitimních souborů (souborů vytvořených balíčky Debianu) nebo vytvořených souborů nastavení nespravovaných <command>dpkg</command>, apod.)."

#, fuzzy
#| msgid "Be careful to not blindly delete everything that <command>cruft</command> might list!"
msgid "Be careful to not blindly delete everything that <command>cruft</command> and <command>cruft-ng</command> might list!"
msgstr "Mějte se na pozoru a slepě namažte vše, co <command>cruft</command> vypíše!"

#, fuzzy
#| msgid "The <emphasis role=\"pkg\">cruft</emphasis> package proposes to list the available files that are not owned by any package. It has some filters (more or less effective, and more or less up to date) to avoid reporting some legitimate files (files generated by Debian packages, or generated configuration files not managed by <command>dpkg</command>, etc.)."
msgid "The <emphasis role=\"pkg\">debsums</emphasis> package allows to check the MD5 hashsum of each file installed by a package against a reference hashsum and can help to determine, which files might have been altered (see <xref linkend=\"sidebar.debsums\" />). Be aware that created files (files generated by Debian packages, or generated configuration files not managed by <command>dpkg</command>, etc.) are not subject to this check."
msgstr "Balíček <emphasis role=\"pkg\">cruft</emphasis> přichází s vypsáním dostupných souborů, které nepatří žádnému balíčku. Má filtry (více či méně účinné a více či méně aktuální) k tomu, aby předešel hlášení legitimních souborů (souborů vytvořených balíčky Debianu) nebo vytvořených souborů nastavení nespravovaných <command>dpkg</command>, apod.)."

msgid "The <emphasis role=\"pkg\">apt-show-versions</emphasis> package provides a tool to check for installed packages without a package source and can help to determine third party packages (see <xref linkend=\"sect.apt-show-versions\" />)."
msgstr ""

msgid "Installing Debian"
msgstr "Instalace Debianu"

msgid "Once all the required information on the current server is known, we can shut it down and begin to install Debian on it."
msgstr "Jakmile jsou všechny informace o současném serveru známy, můžeme jej vypnout a začít na něm s instalací Debianu."

msgid "<primary>architecture</primary>"
msgstr "<primary>architektura</primary>"

msgid "To choose the appropriate version, we must know the computer's architecture. If it is a reasonably recent PC, it is most likely to be amd64 (older PCs were usually i386). In other cases, we can narrow down the possibilities according to the previously used system."
msgstr "Abychom vybrali správnou verzi, je nutné znát architekturu daného počítače. Pokud to je, jak vidno, současné PC, potom to bude pravděpodobně amd64 (starší počítače byly obyčejně i386). V ostatních případech můžeme zúžit výběr podle systému, který byl použit předtím."

#, fuzzy
#| msgid "<xref linkend=\"tab-corresp\" xrefstyle=\"select: label nopage\" /> is not intended to be exhaustive, but may be helpful. In any case, the original documentation for the computer is the most reliable source to find this information."
msgid "<xref linkend=\"tab-corresp\" xrefstyle=\"select: label nopage\" /> is not intended to be exhaustive, but may be helpful. Note that it lists Debian architectures which are no longer supported in the current stable release. In any case, the original documentation for the computer is the most reliable source to find this information."
msgstr "<xref linkend=\"tab-corresp\" xrefstyle=\"select: label nopage\" /> není zamýšleno být vyčerpávající, ale může pomoci. V každém případě, původní dokumentace počítače je nejspolehlivějším zdrojem takovýchto informací."

msgid "Matching operating system and architecture"
msgstr "Spojení správného operačního systému se správnou architekturou"

msgid "Operating System"
msgstr "Operační systém"

msgid "Architecture(s)"
msgstr "Architektura(y)"

msgid "DEC Unix (OSF/1)"
msgstr "DEC Unix (OSF/1)"

msgid "alpha, mipsel"
msgstr "alpha, mipsel"

msgid "HP Unix"
msgstr "HP Unix"

msgid "ia64, hppa"
msgstr "ia64, hppa"

msgid "IBM AIX"
msgstr "IBM AIX"

msgid "powerpc"
msgstr "powerpc"

msgid "Irix"
msgstr "Irix"

msgid "mips"
msgstr "mips"

msgid "OS X"
msgstr "OS X"

msgid "amd64, powerpc, i386"
msgstr "amd64, powerpc, i386"

msgid "z/OS, MVS"
msgstr "z/OS, MVS"

msgid "s390x, s390"
msgstr "s390x, s390"

msgid "Solaris, SunOS"
msgstr "Solaris, SunOS"

msgid "sparc, i386, m68k"
msgstr "sparc, i386, m68k"

msgid "Ultrix"
msgstr "Ultrix"

msgid "VMS"
msgstr "VMS"

msgid "alpha"
msgstr "alpha"

msgid "Windows 95/98/ME"
msgstr "Windows 95/98/ME"

msgid "i386"
msgstr "i386"

msgid "Windows NT/2000"
msgstr "Windows NT/2000"

msgid "i386, alpha, ia64, mipsel"
msgstr "i386, alpha, ia64, mipsel"

msgid "Windows XP / Windows Server 2008"
msgstr "Windows XP / Windows Server 2008"

msgid "i386, amd64, ia64"
msgstr "i386, amd64, ia64"

#, fuzzy
#| msgid "Windows NT/2000"
msgid "Windows RT"
msgstr "Windows NT/2000"

msgid "armel, armhf, arm64"
msgstr ""

#, fuzzy
#| msgid "Windows Vista / Windows 7 / Windows 8"
msgid "Windows Vista / Windows 7-8-10"
msgstr "Windows Vista / Windows 7 / Windows 8"

msgid "i386, amd64"
msgstr "i386, amd64"

msgid "<emphasis>HARDWARE</emphasis> 64 bit PC vs 32 bit PC"
msgstr "<emphasis>HARDWARE</emphasis> 64 bit PC oproti 32 bit PC"

msgid "<primary>amd64</primary>"
msgstr "<primary>amd64</primary>"

msgid "<primary>i386</primary>"
msgstr "<primary>i386</primary>"

msgid "Most recent computers have 64 bit Intel or AMD processors, compatible with older 32 bit processors; the software compiled for “i386” architecture thus works. On the other hand, this compatibility mode does not fully exploit the capabilities of these new processors. This is why Debian provides the “amd64” architecture, which works for recent AMD chips as well as Intel “em64t” processors (including most of the Core series), which are very similar to AMD64."
msgstr "Nejnovější počítače mají 64 bitové procesory Intel nebo AMD, kompatibilní se starými 32 bitovými procesory; software sestavený pro architekturu “i386” proto funguje. Na druhou stranu, tento kompatabilní mód plně nevyužívá schopností těchto nových procesorů. To je důvod, proč Debian poskytuje “amd64” architekturu, která funguje se současnými čipy AMD, stejně jako s procesory “em64t” Intelu (včetně většiny z Core série), které se AMD64 hodně podobají."

msgid "Installing and Configuring the Selected Services"
msgstr "Instalace a nastavení vybraných služeb"

#, fuzzy
#| msgid "Once Debian is installed, we must install and configure one by one all of the services that this computer must host. The new configuration must take into consideration the prior one in order to ensure a smooth transition. All the information collected in the first two steps will be useful to successfully complete this part."
msgid "Once Debian is installed, we need to individually install and configure each of the services that this computer must host. The new configuration must take into consideration the prior one in order to ensure a smooth transition. All the information collected in the first two steps will be useful to successfully complete this part."
msgstr "Jakmile je Debian nainstalován, musíme nainstalovat a nastavit jednu po druhé služby, které má tento počítač hostit. Nové nastavení musí brát v potaz to předešlé, aby byl zajištěn bezproblémový přechod. Všechny informace získané v prvních dvou krocích napomůžou k úspěšnému dokončení této části."

msgid "Install the selected services"
msgstr "Instalace vybraných služeb"

msgid "Prior to jumping into this exercise with both feet, it is strongly recommended that you read the remainder of this book. After that you will have a more precise understanding of how to configure the expected services."
msgstr "Než skočíte do akce oběma nohama, vřele doporučujeme si přečíst zbytek této knihy. Potom lépe porozumíte tomu, jak nastavit očekávané služby."

#~ msgid "<primary>AFP</primary>"
#~ msgstr "<primary>AFP</primary>"
